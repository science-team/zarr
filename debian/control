Source: zarr
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Antonio Valentino <antonio.valentino@tiscali.it>
Section: python
Testsuite: autopkgtest-pkg-pybuild
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-numpy3,
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               pybuild-plugin-pyproject,
               python3-aiohttp <!nocheck>,
               python3-all,
               python3-botocore <!nocheck>,
               python3-crc32c,
               python3-donfig,
               python3-fsspec,
               python3-hatch-vcs,
               python3-hatchling,
               python3-hypothesis <!nocheck>,
               python3-msgpack <!nodoc>,
               python3-numcodecs,
               python3-numpy,
               python3-numpydoc <!nodoc>,
               python3-packaging,
               python3-typing-extensions,
               python3-pytest <!nocheck>,
               python3-pytest-asyncio <!nocheck>,
               python3-requests <!nocheck>,
               python3-rich <!nocheck>,
               python3-sphinx <!nodoc>,
               python3-sphinx-autoapi <!nodoc>,
               python3-sphinx-autobuild <!nodoc>,
               python3-sphinx-copybutton <!nodoc>,
               python3-sphinx-design <!nodoc>,
               python3-sphinx-issues <!nodoc>,
               python3-sphinx-reredirects <!nodoc>,
               python3-pydata-sphinx-theme <!nodoc>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/zarr
Vcs-Git: https://salsa.debian.org/science-team/zarr.git
Homepage: https://github.com/zarr-developers/zarr-python
Rules-Requires-Root: no

Package: python3-zarr
Architecture: all
Depends: python3-crc32c,
         python3-sphinx-copybutton,
         python3-pydata-sphinx-theme,
         ${python3:Depends},
         ${sphinxdoc:Depends},
         ${misc:Depends}
Recommends: python3-aiohttp,
            python3-botocore,
            python3-fsspec,
            python3-requests,
	    python3-rich
Description: chunked, compressed, N-dimensional arrays for Python
 Zarr is a Python package providing an implementation of compressed,
 chunked, N-dimensional arrays, designed for use in parallel
 computing. Some highlights:
 .
   - Create N-dimensional arrays with any NumPy dtype.
   - Chunk arrays along any dimension.
   - Compress chunks using the fast Blosc meta-compressor or
     alternatively using zlib, BZ2 or LZMA.
   - Store arrays in memory, on disk, inside a Zip file, on S3, ...
   - Read an array concurrently from multiple threads or processes.
   - Write to an array concurrently from multiple threads or processes.
   - Organize arrays into hierarchies via groups.
   - Use filters to preprocess data and improve compression.
